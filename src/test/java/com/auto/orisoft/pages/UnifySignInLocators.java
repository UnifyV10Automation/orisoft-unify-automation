package com.auto.orisoft.pages;

import org.openqa.selenium.By;

public class UnifySignInLocators {
	
	
	public final static By customerCode=By.xpath("//input[@id='CUSTOMER_CODE_I']");
	public final static By userId=By.xpath("//input[@id='USERID_I']");
	public final static By password=By.xpath("//input[@id='USERPWD_I_CLND']");
	public final static By signIn=By.xpath("//div[@id='btnSignIn_CD']");
	public final static By passwordSendkeys=By.xpath("//input[@id='USERPWD_I']");
	
}
