package com.auto.orisoft.pages;

import org.openqa.selenium.By;

public class PayrolLocators {

	
	public final static By payroll_slideMenu= By.xpath("//div[@id='mm-1']//li[4]//a[1]");
	public final static By payrollTransactions=By.xpath("//div[@id='mm-12']//li[1]//a[1]");
	public final static By payrollProcess=By.xpath("//div[@id='mm-12']//li[2]//a[1]");
	public final static By payrollRun=By.xpath("//a[contains(text(),'Payroll Run')]");
	
 }
