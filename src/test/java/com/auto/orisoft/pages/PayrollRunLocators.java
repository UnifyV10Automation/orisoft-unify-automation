package com.auto.orisoft.pages;

import org.openqa.selenium.By;

public class PayrollRunLocators {
	public final static By payPeriodDropdownArrow= By.xpath("//td[@id='ddlPayPeriod_B-1']");
	public final static By selectCosmoMyClientPayPeriod= By.xpath("//td[@id='PayPeriod_LBI1T1']");
	public final static By submitProcessPayPeriod= By.xpath("//span[contains(text(),'Submit Process')]");
	public final static By verifyPayrollRunMessage= By.xpath("//div[@class='dx-toast-message']");
}
