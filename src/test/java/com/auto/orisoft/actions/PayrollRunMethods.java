package com.auto.orisoft.actions;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auto.orisoft.pages.PayrollRunLocators;
import com.auto.orisoft.util.LoggerUtils;
import com.auto.orisoft.util.Webdriverutility;

public class PayrollRunMethods {
	public static void moveToPayrollProcess(WebDriver driver) throws Exception {
		LoggerUtils.info("Click on PayrollProcess-> Payroll run ");
		Webdriverutility.waitForElementPresence(driver, PayrollRunLocators.payPeriodDropdownArrow);
		driver.findElement(PayrollRunLocators.payPeriodDropdownArrow).click();
		Webdriverutility.waitForElementPresence(driver, PayrollRunLocators.selectCosmoMyClientPayPeriod);
		driver.findElement(PayrollRunLocators.selectCosmoMyClientPayPeriod).click();
		Webdriverutility.waitForElementPresence(driver, PayrollRunLocators.submitProcessPayPeriod);
		driver.findElement(PayrollRunLocators.submitProcessPayPeriod).click();
		driver.findElement(PayrollRunLocators.submitProcessPayPeriod).click();
		Webdriverutility.waitfor(5);
		CommonTestMethods.isAlertPresent(driver);
		Webdriverutility.waitForElementPresence(driver, PayrollRunLocators.verifyPayrollRunMessage);
		String message = driver.findElement(PayrollRunLocators.verifyPayrollRunMessage).getText();
		Assert.assertTrue(message.contains("scheduled successfully"));
	}
}
