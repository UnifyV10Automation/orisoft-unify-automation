package com.auto.orisoft.actions;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auto.orisoft.pages.CommonLocators;
import com.auto.orisoft.pages.PayrolLocators;
import com.auto.orisoft.pages.UnifySignInLocators;
import com.auto.orisoft.util.LoggerUtils;
import com.auto.orisoft.util.WaitHelperUtil;
import com.auto.orisoft.util.Webdriverutility;

public class CommonTestMethods {

	// method to go to profile screen
	public static void loginFeature(WebDriver driver, String customercode, String userid, String password)
			throws Exception {
		// Code to complete first testcase
		LoggerUtils.info("Entering details for signin on Unify");
		Webdriverutility.waitForElementPresence(driver, UnifySignInLocators.customerCode);
		driver.findElement(UnifySignInLocators.customerCode).sendKeys(customercode);
		Webdriverutility.waitForElementPresence(driver, UnifySignInLocators.userId);
		driver.findElement(UnifySignInLocators.userId).sendKeys(userid);
		Webdriverutility.waitfor(1);
		Webdriverutility.waitForElementPresence(driver, UnifySignInLocators.password);
		driver.findElement(UnifySignInLocators.password).click();
		Webdriverutility.waitfor(2);
		driver.findElement(By.xpath("//input[@id='USERPWD_I']")).sendKeys(password);
		Webdriverutility.waitForElementPresence(driver, UnifySignInLocators.signIn);
		driver.findElement(UnifySignInLocators.signIn).click();
		WaitHelperUtil.waitForLoad(driver, 15000);
		Assert.assertTrue(driver.getCurrentUrl().contains("Dashboard"),
				"logging unsuccessful, current url is " + driver.getCurrentUrl());
	}

	public static void moveToPayroll(WebDriver driver) throws Exception {
		LoggerUtils.info("Click on slide menu");
		Webdriverutility.waitForElementPresence(driver, CommonLocators.SlideMenu);
		driver.findElement(CommonLocators.SlideMenu).click();
		Webdriverutility.waitForElementPresence(driver, PayrolLocators.payroll_slideMenu);
		driver.findElement(PayrolLocators.payroll_slideMenu).click();
	}
	public static void moveToPayrollProcess(WebDriver driver) throws Exception {
		LoggerUtils.info("Click on PayrollProcess ");
		Webdriverutility.waitForElementPresence(driver, PayrolLocators.payrollProcess);
		driver.findElement(PayrolLocators.payrollProcess).click();
	}
	public static void moveToPayrollRun(WebDriver driver) throws Exception {
		LoggerUtils.info("Click on PayrollRun ");
		Webdriverutility.waitForElementPresence(driver, PayrolLocators.payrollRun);
		driver.findElement(PayrolLocators.payrollRun).click();
		WaitHelperUtil.waitForLoad(driver, 15000);
		String url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains("/PayrollProcess/ProcessControlPanel"));
		
	}

//	public static void publishArticle(WebDriver driver,String ArticleLink) throws Exception
//	{
//		Webdriverutility.waitForElementPresence(driver, PublishArticle.ArticleLink);
//		driver.findElement(PublishArticle.ArticleLink).click();
//		WaitHelperUtil.waitForLoad(driver, 15000);
//		 driver.switchTo().window("publish");
//		 Webdriverutility.waitForElementPresence(driver, HeadlineOfArticle.HeadLine);
//		 driver.findElement(HeadlineOfArticle.HeadLine).sendKeys("Hi I am Testing");
//	}
	public static boolean isAlertPresent(WebDriver driver) {

		  boolean presentFlag = false;

		  try {

		   // Check the presence of alert
		   Alert alert = driver.switchTo().alert();
		   // Alert present; set the flag
		   presentFlag = true;
		   // if present consume the alert
		   alert.accept();

		  } catch (NoAlertPresentException ex) {
		   // Alert not present
		   ex.printStackTrace();
		  }

		  return presentFlag;

		 }
}
