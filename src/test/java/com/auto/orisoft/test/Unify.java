package com.auto.orisoft.test;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.auto.orisoft.actions.CommonTestMethods;
import com.auto.orisoft.actions.PayrollRunMethods;
import com.auto.orisoft.base.TestBase;
import com.auto.orisoft.pages.PayrolLocators;
import com.auto.orisoft.pages.PayrollRunLocators;
import com.auto.orisoft.util.WaitHelperUtil;
import com.auto.orisoft.util.Webdriverutility;

public class Unify extends TestBase {

	@Test
	public void Login() throws Exception {
		String testname = Thread.currentThread().getStackTrace()[1].getMethodName();
		createDriver(testname);
		WebDriver driver = TestBase.getDriver();
		driver.get(baseUrl);
		WaitHelperUtil.waitForLoad(driver, 15000);
		CommonTestMethods.loginFeature(driver, customercode, userid, password);
		CommonTestMethods.moveToPayroll(driver);
		CommonTestMethods.moveToPayrollProcess(driver);
		CommonTestMethods.moveToPayrollRun(driver);
		PayrollRunMethods.moveToPayrollProcess(driver);
		driver.get("http://ma-qawb-t1:8188/Hangfire/jobs/processing");
}
}
