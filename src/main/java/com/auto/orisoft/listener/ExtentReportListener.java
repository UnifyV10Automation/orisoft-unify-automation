package com.auto.orisoft.listener;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.ITestAnnotation;

import com.auto.orisoft.base.TestBase;
import com.auto.orisoft.util.LoggerUtils;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ExtentReportListener implements ITestListener, ISuiteListener, IAnnotationTransformer {

	public ExtentReports extentReports = ExtentManager
			.getReporter("." + File.separator + "reports" + File.separator + "OrisoftAutomationReport.html");
	public ExtentTest extentTest;

	// This belongs to ISuiteListener and will execute before the Suite start
	@Override
	public void onStart(ISuite arg0) {
		LoggerUtils.info("About to begin executing Suite " + arg0.getName());
	}

	// This belongs to ISuiteListener and will execute, once the Suite is
	// finished
	@Override
	public void onFinish(ISuite arg0) {
		LoggerUtils.info("About to end executing Suite " + arg0.getName());
		for (String s : Reporter.getOutput()) {
			extentReports.setTestRunnerOutput(s);
		}
		extentReports.flush();
	}

	// This belongs to ITestListener and will execute before starting of Test
	// set/batch
	@Override
	public void onStart(ITestContext arg0) {
	}

	// This belongs to ITestListener and will execute, once the Test set/batch
	// is finished
	@Override
	public void onFinish(ITestContext arg0) {
		Set<ITestResult> failedTests = arg0.getFailedTests().getAllResults();
		for (ITestResult temp : failedTests) {
			ITestNGMethod method = temp.getMethod();
			if (arg0.getFailedTests().getResults(method).size() > 1) {
				failedTests.remove(temp);
			} else {
				if (arg0.getPassedTests().getResults(method).size() > 0) {
					failedTests.remove(temp);
				}
			}
		}
	}

	// This belongs to ITestListener and will execute only when the test is pass
	@Override
	public void onTestSuccess(ITestResult arg0) {
		LoggerUtils logger = (LoggerUtils) arg0.getAttribute("result");
		String[] groups = getGroupsName(arg0);
		if (arg0.getParameters() != null && arg0.getParameters().length > 0) {
			extentTest = extentReports.startTest(arg0.getName());
			extentTest.setStartedTime(getTime(arg0.getStartMillis()));
			if (groups.length > 0) {
				extentTest.assignCategory(groups);
			}
		} else {
			extentTest = extentReports.startTest(arg0.getName());
			extentTest.setStartedTime(getTime(arg0.getStartMillis()));
			if (groups.length > 0) {
				extentTest.assignCategory(groups);
			}
		}
		// taking all steps
		try {
			if (logger != null) {
				for (int i = 0; i < logger.utilList.size(); i++) {
					extentTest.log(LogStatus.INFO, arg0.getName(), logger.utilList.get(i));
				}
			}

		} catch (Throwable e) {
			e.printStackTrace();

		}
		printTestResults(arg0);
		logTestResults(arg0);
		logger.info("Test case :" + arg0.getName() + " got passed");
		// clearing list
		logger.utilList.clear();
	}

	// This belongs to ITestListener and will execute only on the event of fail
	// test
	@Override
	public void onTestFailure(ITestResult result) {
		// This is calling the printTestResults method
		LoggerUtils logger = (LoggerUtils) result.getAttribute("result");
		String[] groups = getGroupsName(result);
		if (result.getParameters() != null && result.getParameters().length > 0) {
			extentTest = extentReports.startTest(result.getName());
			extentTest.setStartedTime(getTime(result.getStartMillis()));
			if (groups.length > 0) {
				extentTest.assignCategory(groups);
			}
		} else {
			extentTest = extentReports.startTest(result.getName());
			extentTest.setStartedTime(getTime(result.getStartMillis()));
			if (groups.length > 0) {
				extentTest.assignCategory(groups);
			}
		}
		// taking all steps
		try {

			if (logger != null) {
				for (int i = 0; i < logger.utilList.size(); i++) {
					extentTest.log(LogStatus.INFO, result.getName(), logger.utilList.get(i));
				}
			}

		} catch (Throwable e) {
			e.printStackTrace();
		}

		printTestResults(result);
		logTestResults(result);
		// clearing list
		logger.utilList.clear();
	}

	// This belongs to ITestListener and will execute before the main test start
	// (@Test)
	@Override
	public void onTestStart(ITestResult arg0) {
	}

	// This belongs to ITestListener and will execute only if any of the main
	// test(@Test) get skipped
	@Override
	public void onTestSkipped(ITestResult arg0) {
		LoggerUtils logger = (LoggerUtils) arg0.getAttribute("result");
		String[] groups = getGroupsName(arg0);
		if (arg0.getParameters() != null && arg0.getParameters().length > 0) {
			extentTest = extentReports.startTest(arg0.getName());
			extentTest.setStartedTime(getTime(arg0.getStartMillis()));
			if (groups.length > 0) {
				extentTest.assignCategory(groups);
			}
		} else {
			extentTest = extentReports.startTest(arg0.getName());
			extentTest.setStartedTime(getTime(arg0.getStartMillis()));
			if (groups.length > 0) {
				extentTest.assignCategory(groups);
			}
		}
		try {

			if (logger != null) {

				for (int i = 0; i < logger.utilList.size(); i++) {
					extentTest.log(LogStatus.INFO, arg0.getName(), logger.utilList.get(i));
				}

			}

		} catch (Throwable e) {
			e.printStackTrace();
		}
		// taking all steps
		printTestResults(arg0);
		logTestResults(arg0);
		// clearing list
		logger.utilList.clear();
	}

	// This will provide the information on the test

	private void printTestResults(ITestResult result) {
		LoggerUtils logger = (LoggerUtils) result.getAttribute("result");
		String status = null;
		switch (result.getStatus()) {
		case ITestResult.SUCCESS:
			status = "Pass";
			break;
		case ITestResult.FAILURE:
			status = "Failed";
			break;
		case ITestResult.SKIP:
			status = "Skipped";
		}
		logger.info("Test Status: " + status);

	}

	@SuppressWarnings("rawtypes")
	@Override
	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
		IRetryAnalyzer retry = annotation.getRetryAnalyzer();
		if (retry == null) {
			annotation.setRetryAnalyzer(RerunFailedTestCases.class);
		}
	}

	/**
	 * user defined methods
	 */

	private Date getTime(long millis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	}

	/**
	 * user defined methods
	 * 
	 * @param result
	 */

	public String getCurrentTimeInstance() {
		Calendar calendar = Calendar.getInstance();
		String currentTimeInstance = "-" + calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-"
				+ calendar.get(Calendar.DAY_OF_MONTH) + "-" + calendar.get(Calendar.HOUR_OF_DAY) + "-"
				+ calendar.get(Calendar.MINUTE) + "-" + calendar.get(Calendar.SECOND) + "-"
				+ calendar.get(Calendar.MILLISECOND);
		return currentTimeInstance;

	}

	/***
	 * @author common method to take screenshot
	 */
	public String takeScreenShot(Object currentClass, ITestResult result) {

		WebDriver augumented = TestBase.getDriver();
		if (augumented != null) {
			String timeStamp = getCurrentTimeInstance();
			String filename = System.getProperty("user.dir") + "/screenshots" + "/" + result.getMethod().getMethodName()
					+ timeStamp;
			// String filename = "." + File.separator + "screenshots" + File.separator +
			// result.getMethod().getMethodName()
			// + timeStamp;
			File f = ((TakesScreenshot) augumented).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(f, new File(filename + ".png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			String imageName = filename + ".png";
			return imageName;
		}
		return null;
	}

	public void logTestResults(ITestResult testResult) {
		switch (testResult.getStatus()) {
		case ITestResult.SUCCESS:
			logTestResultsForPassed(testResult);
			break;
		case ITestResult.FAILURE:
			logTestResultsForFail(testResult);
			break;
		case ITestResult.SKIP:
			logTestResultsForSkip(testResult);
			break;
		}
	}

	public void logTestResultsForPassed(ITestResult testResult) {
		if (testResult.getParameters() != null && testResult.getParameters().length > 0) {
			extentTest.setEndedTime(getTime(testResult.getEndMillis()));
			extentTest.log(LogStatus.PASS, testResult.getName());
		} else {
			extentTest.setEndedTime(getTime(testResult.getEndMillis()));
			extentTest.log(LogStatus.PASS, testResult.getName());
		}
		extentReports.endTest(extentTest);
	}

	public synchronized void logTestResultsForFail(ITestResult testResult) {
		LoggerUtils logger = (LoggerUtils) testResult.getAttribute("result");
		Object currentClass = testResult.getInstance();
		String img = null;
		try {
			String imageName = takeScreenShot(currentClass, testResult);
			try {
				img = extentTest.addScreenCapture(imageName);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (testResult.getParameters() != null && testResult.getParameters().length > 0) {
				extentTest.setEndedTime(getTime(testResult.getEndMillis()));
				extentTest.log(LogStatus.FAIL, testResult.getName(), testResult.getThrowable());
				extentTest.log(LogStatus.FAIL, testResult.getName(), img);
			} else {
				extentTest.setEndedTime(getTime(testResult.getEndMillis()));
				extentTest.log(LogStatus.FAIL, testResult.getName(), testResult.getThrowable());
				extentTest.log(LogStatus.FAIL, testResult.getName(), img);
			}
//				SlackAlert.postToSlack(testResult.getName() + ":" + testResult.getThrowable().toString());
			logger.error("Test case :" + testResult.getName() + " got failed... Failure Reason:"
					+ testResult.getThrowable());
			extentReports.endTest(extentTest);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void logTestResultsForSkip(ITestResult testResult) {
		LoggerUtils logger = (LoggerUtils) testResult.getAttribute("result");
		Object currentClass = testResult.getInstance();
		String img = null;
		try {
			String imageName = takeScreenShot(currentClass, testResult);
			try {
				img = extentTest.addScreenCapture(imageName);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (testResult.getParameters() != null && testResult.getParameters().length > 0) {
				try {
					extentTest.setEndedTime(getTime(testResult.getEndMillis()));
					extentTest.log(LogStatus.SKIP, testResult.getName(), testResult.getThrowable());
				} catch (NullPointerException e) {
					extentTest.setEndedTime(getTime(testResult.getEndMillis()));
					extentTest.log(LogStatus.SKIP, testResult.getName(), "Test case skipped");
				}
				extentTest.setEndedTime(getTime(testResult.getEndMillis()));
				extentTest.log(LogStatus.SKIP, testResult.getName(), img);
			} else {
				try {
					extentTest.setEndedTime(getTime(testResult.getEndMillis()));
					extentTest.log(LogStatus.SKIP, testResult.getName(), testResult.getThrowable());
				} catch (NullPointerException e) {
					extentTest.setEndedTime(getTime(testResult.getEndMillis()));
					extentTest.log(LogStatus.SKIP, testResult.getName(), "Test Case skipped");
				}
				extentTest.setEndedTime(getTime(testResult.getEndMillis()));
				extentTest.log(LogStatus.SKIP, testResult.getName(), img);
			}

			logger.error("Test case :" + testResult.getName() + " got skipped... skipped Reason:"
					+ testResult.getThrowable());
			extentReports.endTest(extentTest);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String[] getGroupsName(ITestResult testResult) {
		return testResult.getMethod().getGroups();
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	}
}
