package com.auto.orisoft.listener;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.http.util.TextUtils;

import com.auto.orisoft.util.ConfigReader;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;

public class ExtentManager {
	private static ExtentReports extent;
	public static final String CONFIG_KEY_REPORT_REPLACE = "reportReplaceExisting";
	public static ConfigReader reportProperty = new ConfigReader();

	public static ExtentReports getReporter(String filePath) {
		if (extent == null) {
			if (TextUtils.isEmpty(getReplaceExistingValue())) {
				extent = new ExtentReports(filePath, true, NetworkMode.ONLINE);
			} else {
				extent = new ExtentReports(filePath, Boolean.valueOf(getReplaceExistingValue()), NetworkMode.ONLINE);
			}
			File file = getReportConfig();
			if (file != null) {
				extent.loadConfig(file);
			} else {
				extent.config().documentTitle("orisoft Automation Report").reportName("Automation Report");
			}

		}
		extent = addMobSystemInfo(extent);

		return extent;
	}

	// To add Mobile Environment info in the report

	private static ExtentReports addMobSystemInfo(ExtentReports extent) {
		Properties prop = new Properties();
		File f = null;
		try {
			f = new File(System.getProperty("user.dir") + "/config/" + "config.properties");
		} catch (Exception e) {
			System.out.println("Unable to get report config file");
		}

		try {
			prop.load(new FileInputStream(f));

			if (prop.containsKey("Platform"))
				extent.addSystemInfo("Execution Platform", prop.getProperty("Platform"));

			if (prop.getProperty("Platform").equalsIgnoreCase("mobile")) {
				if (prop.containsKey("DeviceName"))
					extent.addSystemInfo("Mobile Device Name", prop.getProperty("DeviceName"));
				if (prop.containsKey("PlatformVersion"))
					extent.addSystemInfo("Mobile OS version", prop.getProperty("PlatformVersion"));
				if (prop.containsKey("appPackage"))
					extent.addSystemInfo("Apk package", prop.getProperty("appPackage"));
			}

			if (prop.getProperty("Platform").equalsIgnoreCase("windows")) {
				if (prop.containsKey("DeviceName"))
					extent.addSystemInfo("Operating System Name", prop.getProperty("OS_Name"));
			}

			if (prop.getProperty("Platform").equalsIgnoreCase("mac")) {
				if (prop.containsKey("DeviceName"))
					extent.addSystemInfo("Operating System Name", prop.getProperty("OS_Name"));
			}

			if (prop.getProperty("Platform").equalsIgnoreCase("docker")) {
				if (prop.containsKey("DeviceName"))
					extent.addSystemInfo("Operating System Name", prop.getProperty("OS_Name"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return extent;
	}

	private static File getReportConfig() {
		File f = null;
		try {
			f = new File(System.getProperty("user.dir") + "/config/" + "reportconfig.xml");
		} catch (Exception e) {
			System.out.println("Unable to get report config file");
		}

		return f;
	}

	private static String getReplaceExistingValue() {
		try {
			String reportReplaceExisting = ConfigReader.getProperty(CONFIG_KEY_REPORT_REPLACE);
			return reportReplaceExisting;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;

	}
}