package com.auto.orisoft.listener;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

import com.auto.orisoft.util.ConfigReader;
import com.auto.orisoft.util.LoggerUtils;


public class RerunFailedTestCases implements IRetryAnalyzer {
	private int retryCount = 0;
	
	
	private int maxRetryCount = Integer
			.parseInt(ConfigReader.getProperty("maxRetryCount"));

	// Below method returns 'true' if the test method has to be retried else
	// 'false'
	// and it takes the 'Result' as parameter of the test method that just ran
	@Override
	public boolean retry(ITestResult result) {
		LoggerUtils logger = (LoggerUtils) result.getAttribute("result");
		if (!result.isSuccess()) {
			if (retryCount < maxRetryCount) {
				String log = "Retrying test " + result.getName() + " with status "
						+ getResultStatusName(result.getStatus()) + " for the " + (retryCount + 1) + " time(s).";
				logger.info(log);
				retryCount++;
				return true;
			}
		}

		return false;
	}

	public String getResultStatusName(int status) {
		String resultName = null;
		if (status == ITestResult.SUCCESS)
			resultName = "SUCCESS";
		if (status == ITestResult.FAILURE)
			resultName = "FAILURE";
		if (status == ITestResult.SKIP)
			resultName = "SKIP";
		return resultName;
	}

}
