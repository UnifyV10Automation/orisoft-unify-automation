package com.auto.orisoft.base;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.auto.orisoft.util.LoggerUtils;
import com.auto.orisoft.util.PropertyUtils;

public class TestBase {

	public static PropertyUtils reader = new PropertyUtils("config.properties");

	public static String baseUrl = reader.getProperty("baseUrl");
	public static WebDriver driver; 
	public static String customercode = reader.getProperty("customercode");
	public static String userid = reader.getProperty("userid");
	public static String password = reader.getProperty("password");
	

	public static WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public synchronized void createDriver(String methodName) {
		DesiredCapabilities caps = DesiredCapabilities.chrome();
		ChromeOptions options = new ChromeOptions();
		caps.setCapability(ChromeOptions.CAPABILITY, options);
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		setDriver(driver);

	}

	@BeforeMethod()
	public void beforeTest(Method method) throws Exception {
		String templateName = method.getName();
		LoggerUtils.info("Executing " + templateName);
		
		
	}

	@AfterMethod()
	public void afterTest() throws Exception {
		WebDriver driver = getDriver();
		if (driver != null) {
			driver.quit();
		}

	}
}
