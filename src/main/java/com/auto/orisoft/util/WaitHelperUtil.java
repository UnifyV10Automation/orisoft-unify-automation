package com.auto.orisoft.util;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitHelperUtil {

	private static final int DEFAULT_TIMEOUT = 10;
	public static final long Expicit_wait_time = 100;

	public static boolean isElementDisplayed(WebDriver driver, WebElement element) {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		boolean elementDisplayed = false;
		try {
			if (element.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
		}
		return elementDisplayed;
	}

	public static void waitForLoad(WebDriver driver, int timeout) {
		ExpectedCondition<Boolean> pageLoadCondition = (__) -> ((JavascriptExecutor) driver)
				.executeScript("return document.readyState").equals("complete");
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		try {
			wait.until(pageLoadCondition);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static WebDriverWait getWait(int timeOutInSeconds, int pollingEveryInMiliSec, WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
		wait.pollingEvery(pollingEveryInMiliSec, TimeUnit.MILLISECONDS);
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(ElementNotVisibleException.class);
		wait.ignoring(StaleElementReferenceException.class);
		wait.ignoring(NoSuchFrameException.class);
		return wait;

	}

	public static void setImplicitWait(long timeout, TimeUnit unit, WebDriver driver) {
		driver.manage().timeouts().implicitlyWait(timeout, unit == null ? TimeUnit.SECONDS : unit);
	}

	public static void waitForElementVisible(WebElement element, int timeOutInSeconds, int pollingTimeInMiliSec,
			WebDriver driver) {
		setImplicitWait(1, TimeUnit.SECONDS, driver);
		WebDriverWait wait = getWait(timeOutInSeconds, pollingTimeInMiliSec, driver);
		wait.until(ExpectedConditions.visibilityOf(element));
		setImplicitWait(ConfigReader.getImplicitWait(), TimeUnit.SECONDS, driver);
	}

	public static void waitForElementToDisappear(WebElement element, int timeOutInSeconds, int pollingTimeInMiliSec,
			WebDriver driver) {
		try {
			setImplicitWait(1, TimeUnit.SECONDS, driver);
			WebDriverWait wait = getWait(timeOutInSeconds, pollingTimeInMiliSec, driver);
			wait.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(element)));
			setImplicitWait(ConfigReader.getImplicitWait(), TimeUnit.SECONDS, driver);
		} catch (Exception e) {
			LoggerUtils.info(element.toString() + " not available");
		}
	}

	public static void waitForElementPresence(WebDriver driver, WebElement element, int timeout) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static List<WebElement> waitForAllElementsPresenceAndReturn(By locator, WebDriver driver) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIMEOUT);
		return wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
	}

	public static void waitForElementPresence(WebDriver driver, WebElement element) throws Exception {
		waitForElementPresence(driver, element, DEFAULT_TIMEOUT);
	}

	/**
	 * for wait of an element, Pass Seconds
	 */
	public static void waitfor(int seconds) throws InterruptedException {
		Thread.sleep(seconds * 1000);
	}

}
