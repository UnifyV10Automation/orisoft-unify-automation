package com.auto.orisoft.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.Assert;

/**
 * Simple property utils class which can be used to read and write from a
 * property file user just needs to give file path of property file located.
 * 
 * @author Vishakha Ashri
 *
 */
public class PropertyUtils {
	private Properties props = new Properties();

	public PropertyUtils(String filename) {

		loadPropertyFile(System.getProperty("user.dir") + "/config/" + filename);
	}

	public void loadPropertyFile(String propertyFileName) {
		try {
			props.load(new FileInputStream(propertyFileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Assert.fail("Unable to load file!", e);
			// e.printStackTrace();
		}
	}

	public String getProperty(String propertyKey) {
		String propertyValue = props.getProperty(propertyKey.trim());

		if (propertyValue == null || propertyValue.trim().length() == 0) {
			// Log error message
			LoggerUtils.error(propertyKey + " Not available in Config File");
		}

		return propertyValue;
	}

	public void setProperty(String propertyKey, String value) {
		props.setProperty(propertyKey, value);
	}

	public int getPageLoadTimeOut() {
		return Integer.parseInt(props.getProperty("PageLoadTimeOut"));
	}

	public int getImplicitWait() {
		return Integer.parseInt(props.getProperty("ImplicitWait"));
	}

	public int getExplicitWait() {
		return Integer.parseInt(props.getProperty("ExplicitWait"));
	}
}
