package com.auto.orisoft.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import com.auto.orisoft.base.TestBase;




public class ConfigReader extends TestBase{

	private static Properties properties = new Properties();
	private final String propertyFilePath= "config/config.properties";
	

	public ConfigReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}
	
	public static String getProperty(String propertyKey) {
		String propertyValue = properties.getProperty(propertyKey.trim());
		if (propertyValue == null || propertyValue.trim().length() == 0) {
			// Log error message
			LoggerUtils.error(propertyKey + " Not available in Config File");
		}

		return propertyValue;
	}
	
	public static void setProperty(String propertyKey, String value) {
		properties.setProperty(propertyKey, value);
	}
	
	public static int getPageLoadTimeOut() {
		return Integer.parseInt(properties.getProperty("PageLoadTimeOut"));
	}

	public static int getImplicitWait() {
		return Integer.parseInt(properties.getProperty("ImplcitWait"));
	}

	public static int getExplicitWait() {
		return Integer.parseInt(properties.getProperty("ExplicitWait"));
	}


}
