package com.auto.orisoft.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Webdriverutility {

	private static final int DEFAULT_TIMEOUT = 60;

	public static void waitForElementPresence(WebDriver driver, By locator) throws Exception {
		waitForElementPresence(driver, locator, DEFAULT_TIMEOUT);
	}

	public static void waitForElementPresence(WebDriver driver, By locator, int timeout) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void waitfor(int seconds) throws InterruptedException {
		Thread.sleep(seconds * 1000);
	}
}
